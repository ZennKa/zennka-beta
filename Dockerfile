FROM node:latest

WORKDIR usr/src

COPY ./ ./

CMD ["npm", "install"]
CMD ["npm", "start"]